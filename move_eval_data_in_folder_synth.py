#!/bin/env python3
import glob
import json
import os
DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset/Synthetic/"
os.chdir(DATASET_DIRECTORY)
WEATHER_KITTI_DIRECTORY = "/home/andyzhang/Bureau/DATASET_EXTERNE/weather_kitti"
FOGGY_CITYSCAPES_DIRECTORY = "/home/andyzhang/Bureau/DATASET_EXTERNE/foggy_cityscapes"


def get_all_eval_set_list():
    """
    This function retrieve all the json files that contains the list of the eval data for each context
    :return: the list of the json file for every context that contains all the evaluation data
    """
    context_eval = []
    for file in glob.glob("{}file_list/*test.json".format(DATASET_DIRECTORY)):
        context_eval.append(file)
    return context_eval


def copy_data_context(data_list, context):
    """
    Copy the data (image/annotations) of the eval set into a new folder
    This folders created will be uploaded in NAS
    :param data_list: directory where the .json file that contain the list of data is saved
    :param context: the context we consider
    :return:
    """
    print("Copying images and annotation of the evaluation set for the context :", context, "...")
    with open(data_list, "r") as current_annotation:
        annotations_list = json.load(current_annotation)

    for annotation_path in annotations_list:
        # Get all the need path for the copy
        image_path = annotation_path.replace("Annotations_caipy", "Images")
        image_path_jpg = image_path.replace(".json", ".jpg")

        # Rename the context
        if context == "rain_day":
            renamed_context = "rainy_day"
        else:
            renamed_context = "foggy_day"

        for file in glob.glob("data/{}/*".format(renamed_context)):
            # Check first if exists, if yes do copy
            # We need to do this because of the images that are in jpg format and the other that are in png format
            if file.split("_")[-3] != "fog" and file.split("_")[-3] != "rain":
                intensity_folder = file.split("_")[-3] + "_" + file.split("_")[-2]
                intensity_val = file.split("_")[-3] + "." + file.split("_")[-2]
            else:
                intensity_folder = file.split("_")[-2]
                intensity_val = file.split("_")[-2]
            annotation_path_intensity = annotation_path.replace("_0_", "_{}_".format(intensity_folder))
            image_path_jpg_intensity = image_path_jpg.replace("_0_", "_{}_".format(intensity_folder))
            if context == "fog_day":
                if intensity_val != "0":
                    image_path_jpg_intensity = image_path_jpg_intensity.replace("leftImg8bit",
                                                                                "leftImg8bit_foggy_beta_{}".
                                                                                format(intensity_val))
                    annotation_path_intensity = annotation_path_intensity.replace("leftImg8bit",
                                                                                  "leftImg8bit_foggy_beta_{}".format(
                                                                                      intensity_val))

            copy_annotation_command = "test -f {} && cp {} {}/Annotations/".format(annotation_path_intensity,
                                                                                   annotation_path_intensity, file)
            copy_img_jpg_command = "test -f {} && cp {} {}/Images/".format(image_path_jpg_intensity,
                                                                           image_path_jpg_intensity, file)

            # We copy everything
            os.system(copy_annotation_command)
            os.system(copy_img_jpg_command)


def create_context_folders(context):
    """
    This function will create all the folders' context/ context/Images/ and context/Annotations/ where the evaluation
    data will be copied
    :param context: the context we consider
    :return:
    """
    intesity_folder = []
    if context == "rain_day":
        context = "rainy_day"
        for file in glob.glob("{}/*_caipy".format(WEATHER_KITTI_DIRECTORY)):
            intesity_folder.append(file)
    else:
        context = "foggy_day"
        for file in glob.glob("{}/*_caipy".format(FOGGY_CITYSCAPES_DIRECTORY)):
            intesity_folder.append(file)
    context_dir = "data/{}".format(context)
    os.mkdir(context_dir)

    for intensity_file in intesity_folder:
        intensity_file = intensity_file.split("/")[-1]
        intensity_folder = "{}/{}".format(context_dir, intensity_file)
        image_folder = "{}/Images".format(intensity_folder)
        annotation_folder = "{}/Annotations".format(intensity_folder)
        os.mkdir(intensity_folder)
        os.mkdir(image_folder)
        os.mkdir(annotation_folder)


def copy_all_data():
    """
    This function will use copy_data_context for all the context we consider
    :return:
    """
    os.system("rm -r data")
    os.mkdir("data")
    context_eval = get_all_eval_set_list()
    for data_list in context_eval:
        context = data_list.split("/")[-1].replace("_test.json", "")
        create_context_folders(context)
        copy_data_context(data_list, context)


if __name__ == "__main__":
    copy_all_data()
