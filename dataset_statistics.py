#!/bin/env python3

import sys
import json
sys.path.insert(1, "/home/andyzhang/convert_data_to_caipy")
from statistics_analyzer import merge_dict

CONTEXTS = ["clear_night", "foggy_day", "foggy_night", "interior", "rainy_day", "rainy_night", "snowy_day",
            "snowy_night", "sunny_day"]

IGNORE = ["ignore areas", "ignored class"]


def get_context_statistics(context_time, train_test):
    """
    This function opens the json file that contains the path of all the annotations of the eval set, open all the
    annotations one by one and get the statistics in a dict
    :param train_test: string "train" or "test" to get the statistics of the train or the test set
    :param context_time: context_time couple, the possible values are in CONTEXTS
    :return: the dictionary containing the statistics of this set
    """
    eval_annotation_file = "{}_{}.json".format(context_time, train_test)
    with open(eval_annotation_file, "r") as current_annotation:
        eval_list_paths = json.load(current_annotation)

    dataset_statistic = dict()

    dataset_statistic['nb_images'] = len(eval_list_paths)
    for annotation_path in eval_list_paths:
        with open(annotation_path, "r") as current_annotation:
            annotation = json.load(current_annotation)
        for obj in annotation['annotations']:
            category_str = obj['category_str']
            if category_str in dataset_statistic.keys():
                dataset_statistic[category_str] += 1
            else:
                dataset_statistic[category_str] = 1
    return dataset_statistic


def get_number_of_object_statistics(context_time, train_test):
    """
    Build a dict that contains the statistics of the dataset for the number of objets on every image
    the keys are more_5 or less_5 and the values are the ratio of images that has more or less than 5 objects
    :param context_time: the context we consider
    :param train_test: the training or the test set
    :return:
    """
    eval_annotation_file = "{}_{}.json".format(context_time, train_test)
    with open(eval_annotation_file, "r") as current_annotation:
        eval_list_paths = json.load(current_annotation)

    dataset_statistic = dict()
    dataset_statistic["more_5"] = 0
    dataset_statistic["less_5"] = 0
    dataset_statistic["more_30"] = 0  # More than 30 objects is a crowd
    for annotation_path in eval_list_paths:
        with open(annotation_path, "r") as current_annotation:
            annotation = json.load(current_annotation)
        nb_obj = len(annotation['annotations'])
        if nb_obj <= 5:
            dataset_statistic["less_5"] += 1
        else:
            dataset_statistic["more_5"] += 1

        if nb_obj >= 30:
            dataset_statistic["more_30"] += 1

    nb_images = len(eval_list_paths)
    for key in dataset_statistic.keys():
        dataset_statistic[key] = 100 * dataset_statistic[key] / nb_images

    return dataset_statistic


def get_view_statistics(context_time, train_test):
    """
    Build a dict that contains the statistics of the dataset for the view of every images
    the keys are front-view or cctv and the values are the ratio of images that has the given view
    :param context_time: the context we consider
    :param train_test: the training or the test set
    :return:
    """
    eval_annotation_file = "{}_{}.json".format(context_time, train_test)
    with open(eval_annotation_file, "r") as current_annotation:
        eval_list_paths = json.load(current_annotation)

    dataset_statistic = dict()

    for annotation_path in eval_list_paths:
        with open(annotation_path, "r") as current_annotation:
            annotation = json.load(current_annotation)
        view = annotation['image']['tags']["view"]

        if view in dataset_statistic.keys():
            dataset_statistic[view] += 1
        else:
            dataset_statistic[view] = 1

    nb_images = len(eval_list_paths)
    for key in dataset_statistic.keys():
        dataset_statistic[key] = 100 * dataset_statistic[key] / nb_images

    return dataset_statistic


def get_size_of_object_statistics(context_time, train_test):
    """
    Build a dict that contains the statistics of the dataset
    the keys are big or small and the values are the ratio of images that hare big or small
    :param context_time:
    :param train_test:
    :return:
    """
    eval_annotation_file = "{}_{}.json".format(context_time, train_test)
    with open(eval_annotation_file, "r") as current_annotation:
        eval_list_paths = json.load(current_annotation)

    dataset_statistic = dict()
    dataset_statistic["big"] = 0
    dataset_statistic["small"] = 0
    nb_obj = 0
    for annotation_path in eval_list_paths:
        with open(annotation_path, "r") as current_annotation:
            annotation = json.load(current_annotation)
        width = int(annotation['image']['width'])
        height = int(annotation['image']['height'])
        img_area = width * height
        nb_obj += len(annotation['annotations'])
        for objects in annotation['annotations']:
            object_width = objects['bbox'][2]
            object_height = objects['bbox'][3]
            object_str = objects['category_str']
            object_area = object_width * object_height
            # The ignored class and the ignore areas are not object bbox we want to take into account
            if object_str not in IGNORE:
                if object_area <= img_area/200:
                    dataset_statistic["small"] += 1
                else:
                    dataset_statistic["big"] += 1

    nb_obj = dataset_statistic["small"] + dataset_statistic["big"]
    dataset_statistic["small"] = 100 * dataset_statistic["small"]/nb_obj
    dataset_statistic["big"] = 100 * dataset_statistic["big"] / nb_obj

    return dataset_statistic


def get_global_statistics(context_time):
    """
    This function get global statistics from the eval plus the train set
    :param context_time: the context_time value (rainy_day for example)
    :return: The dict containing the global statistics
    """
    eval_stats = get_context_statistics(context_time, "test")
    train_stats = get_context_statistics(context_time, "train")
    global_stats = merge_dict(eval_stats, train_stats)
    return global_stats


def get_eval_ratio(context_time):
    """
    Get the ratio of each class in the eval set compared to the entire dataset
    :param context_time: the context_time value (rainy_day for example)
    :return: returns the statistics dict where the keys are the class and the values are the ration in percentage of
    the classes in the eval set compared to the train set
    """
    eval_stats = get_context_statistics(context_time, "test")
    global_stats = get_global_statistics(context_time)
    ratio_stats = dict()
    for key in eval_stats.keys():
        ratio_stats[key] = 100 * eval_stats[key]/global_stats[key]

    return ratio_stats
