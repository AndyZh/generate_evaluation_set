#!/bin/env python3
import glob
import json
import os
DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset/"
os.chdir(DATASET_DIRECTORY)


def modify_image_id_annotation(annotation_path):
    """
    Modify the image index of the object annotations in annotation_path
    :param annotation_path: The annotation path
    :param id: The new image id of this annotation
    :return:
    """
    with open(annotation_path, "r") as current_annotation:
        annotations_dict = json.load(current_annotation)
    image_id = annotations_dict['image']['id']
    nb_object = len(annotations_dict['annotations'])
    for index_obj in range(nb_object):
        annotations_dict['annotations'][index_obj]['image_id'] = image_id
    with open(annotation_path, "w") as outfile:
        print("Modifying objects image_id in :", annotation_path)
        json.dump(annotations_dict, outfile)


def reset_object_ids_real():
    """
    For all evaluation sub set, reset the objects ids
    :return:
    """
    for context in glob.glob(f"{DATASET_DIRECTORY}Real/data/*"):
        for annotation in glob.glob(f"{context}/Annotations/*.json"):
            modify_image_id_annotation(annotation)


def reset_object_ids_synth():
    """
    For all evaluation sub set and batch of the synthetic sets, reset the objects ids to have a unique id for each obj
    :return:
    """
    for context in glob.glob(f"{DATASET_DIRECTORY}Synthetic/data/*/*"):
        for annotation in glob.glob(f"{context}/Annotations/*.json"):
            modify_image_id_annotation(annotation)


def reset_object_ids(annotation_list):
    """
    Reset the image ids of the objects for the dataset given by the list of its annotation
    :param annotation_list: the list of annotations
    :return:
    """
    for annotation in annotation_list:
        modify_image_id_annotation(annotation)


if __name__ == "__main__":
    #reset_object_ids_real()
    reset_object_ids_synth()
