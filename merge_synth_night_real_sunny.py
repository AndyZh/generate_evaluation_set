#!/usr/bin/env python3
import os
from sklearn.model_selection import train_test_split
import numpy as np

DATASETS_DIRECTORY = "/home/andyzhang/Bureau"
MERGED_DATASET = f"{DATASETS_DIRECTORY}/sunny_day_with_night_generated"
NIGHT_DATASET = f"{DATASETS_DIRECTORY}/night_generated_training"

TRAIN_VALID_FILE = [f"{MERGED_DATASET}/train.txt",
                    f"{MERGED_DATASET}/val.txt"]

TRAIN_VAL_FOLDER = ["train", "valid"]

def add_synth_night_data_to_sunny_day_data():
    """
    This functions add the night generated dataset to the sunny day dataset
    :return:
    """
    for i, file_path in enumerate(TRAIN_VALID_FILE):
        output_lines = []
        train_val = TRAIN_VAL_FOLDER[i]
        with open(file_path, 'r') as file:
            data = file.read().split('\n')
            data = np.array(data)[:-1]  # convert array to numpy type array
            x_train_night, x_train_day = train_test_split(data, test_size=0.6)

            # Keep 60% of the day images
            for line in x_train_day:
                output_lines.append(f"{line}\n")

            # Replace the rest of the day images with the night images
            for line in x_train_night:
                output_lines.append(line.replace(".jpg", "_night.jpg\n"))
                day_file_name = line.split(" ")[0].replace("\n", "")
                file_name = line.split(" ")[0].replace("\n", "").replace(".jpg", "_night.jpg")
                annotation_name = file_name.replace("_night.jpg", ".txt")
                new_annotation_name = file_name.replace("jpg", "txt")
                print(f"Replacing {day_file_name} ...")
                delete_old_image_command = f"rm {MERGED_DATASET}/{train_val}/{day_file_name}"
                copy_night_image_command = f"cp {NIGHT_DATASET}/Images/{file_name} {MERGED_DATASET}/{train_val}/"
                move_night_annotation_command = f"mv {MERGED_DATASET}/{train_val}/{annotation_name} " \
                                                      f"{MERGED_DATASET}/{train_val}/{new_annotation_name}"

                os.system(delete_old_image_command)
                os.system(copy_night_image_command)
                os.system(move_night_annotation_command)

        with open(file_path, 'w') as file:
            file.writelines(output_lines)


if __name__ == "__main__":
    add_synth_night_data_to_sunny_day_data()
