#!/bin/env python3
import glob
import json
import os
DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset/"
os.chdir(DATASET_DIRECTORY)


def modify_image_id_annotation(annotation_path, index_image):
    """
    Modify the image index of the annotation in annotation_path into index_image
    :param annotation_path: The annotation path
    :param id: The new image id of this annotation
    :return:
    """
    with open(annotation_path, "r") as current_annotation:
        annotations_dict = json.load(current_annotation)
    # Change image id
    annotations_dict['image']['id'] = index_image

    # Change image id in object annotations
    nb_images = len(annotations_dict['annotations'])
    for index_obj in range(nb_images):
        annotations_dict['annotations'][index_obj]['image_id'] = index_image

    with open(annotation_path, "w") as outfile:
        print("Modifying image index in :", annotation_path)
        json.dump(annotations_dict, outfile)


def reset_image_ids_real():
    """
    For all evaluation sub set, reset the image ids to have a unique image id
    :return:
    """
    for context in glob.glob(f"{DATASET_DIRECTORY}Real/data/*"):
        index_image = 0
        for annotation in glob.glob(f"{context}/Annotations/*.json"):
            modify_image_id_annotation(annotation, index_image)
            index_image += 1


def reset_image_ids_synth():
    """
    For all evaluation sub set and batch of the synthetic sets, reset the image ids to have a unique image id
    :return:
    """
    for context in glob.glob(f"{DATASET_DIRECTORY}Synthetic/data/*/*"):
        index_image = 0
        for annotation in glob.glob(f"{context}/Annotations/*.json"):
            modify_image_id_annotation(annotation, index_image)
            index_image += 1


def reset_image_ids(annotation_list):
    """
    Reset the image ids for the dataset given by the list of its annotation
    :param annotation_list: the list of annotations
    :return:
    """
    index_image = 0
    for annotation in annotation_list:
        modify_image_id_annotation(annotation, index_image)
        index_image += 1


if __name__ == "__main__":
    reset_image_ids_real()
    reset_image_ids_synth()
