#!/bin/env python3
import sys
import glob

sys.path.insert(1, "/home/andyzhang/convert_data_to_caipy")
from sklearn.model_selection import train_test_split
import json
from dataset_statistics import get_eval_ratio, get_number_of_object_statistics, \
    get_size_of_object_statistics

DESKTOP_PATH = "/home/andyzhang/Bureau/complete_dataset"

# Dataset names
DATASETS = ["P2C", "Mall", "Campus_Avisio", "Acrelec_midi_neige", "Acrelec", "AAU_RainSnow", "DAWN", "bdd100k",
            "foggy_cityscapes", "foggy_driving", "MOT20Det", "UAV-benchmark-M", "weather_kitti"]

DATASETS_INTERNE = ["P2C", "Mall", "Campus_Avisio", "Acrelec_midi_neige", "Acrelec"]
DATASETS_EXTERNE = ["AAU_RainSnow", "DAWN", "bdd100k", "foggy_cityscapes", "foggy_driving", "MOT20Det",
                    "UAV-benchmark-M", "weather_kitti"]

# The names of the dataset that contain frames of videos
DATASET_WITH_BATCH = ["Acrelec", "Acrelec_midi_neige", "Campus_Avisio", "P2C", "AAU_RainSnow"]

# The datasets that contains synthetic images
DATASET_SYNTHETIC = ["foggy_cityscapes", "weather_kitti"]

WEATHER = ["rainy", "snowy", "clear", "foggy", "sunny"]
TIME = ["day", "night"]

CONTEXTS = ["foggy_day", "foggy_night", "interior", "rainy_day", "rainy_night", "snowy_day",
            "snowy_night", "clear_night", "sunny_day"]

# Set of conditions for a good distributed dataset
CONDITIONS_TO_VERIFY = dict()

# Because of the amount of data and the diversity of the datasets, some conditions will be harder to verify depending
# on the contexts. The values are intervals where the values needs to be in to verify the conditions
# - nb_img : condition of the percentage of the number of image we take for the eval
# - class_distrib : condition of the percentage of the bbox for the class sedan and person
# - nb_obj : condition of the percentage of images will less than 5 objects
# - obj_size : condition on the number of bbox that are "big"

# Conditions to verify for real data
CONDITIONS_TO_VERIFY['clear_night'] = dict()
CONDITIONS_TO_VERIFY['clear_night']['nb_img'] = [16, 24]
CONDITIONS_TO_VERIFY['clear_night']['class_distrib'] = [0, 100]  # Because of all pedestrian that is in two videos.
CONDITIONS_TO_VERIFY['clear_night']['nb_obj'] = [0, 100]
CONDITIONS_TO_VERIFY['clear_night']['obj_size'] = [40, 60]

CONDITIONS_TO_VERIFY['foggy_day'] = dict()
CONDITIONS_TO_VERIFY['foggy_day']['nb_img'] = [16, 24]
CONDITIONS_TO_VERIFY['foggy_day']['class_distrib'] = [10, 30]
CONDITIONS_TO_VERIFY['foggy_day']['nb_obj'] = [20, 70]
CONDITIONS_TO_VERIFY['foggy_day']['obj_size'] = [40, 60]

CONDITIONS_TO_VERIFY['foggy_night'] = dict()
CONDITIONS_TO_VERIFY['foggy_night']['nb_img'] = [16, 24]
CONDITIONS_TO_VERIFY['foggy_night']['class_distrib'] = [10, 30]
CONDITIONS_TO_VERIFY['foggy_night']['nb_obj'] = [20, 70]
CONDITIONS_TO_VERIFY['foggy_night']['obj_size'] = [40, 60]

CONDITIONS_TO_VERIFY['interior'] = dict()
CONDITIONS_TO_VERIFY['interior']['nb_img'] = [16, 27]
# Ignore this conditions, the class sedan has few objects because of the class sedan in this context (few cars inside)
CONDITIONS_TO_VERIFY['interior']['class_distrib'] = [0, 100]
CONDITIONS_TO_VERIFY['interior']['nb_obj'] = [40, 60]
CONDITIONS_TO_VERIFY['interior']['obj_size'] = [40, 60]

CONDITIONS_TO_VERIFY['rainy_day'] = dict()
CONDITIONS_TO_VERIFY['rainy_day']['nb_img'] = [16, 24]
CONDITIONS_TO_VERIFY['rainy_day']['class_distrib'] = [13, 27]
CONDITIONS_TO_VERIFY['rainy_day']['nb_obj'] = [20, 80]
CONDITIONS_TO_VERIFY['rainy_day']['obj_size'] = [40, 60]

CONDITIONS_TO_VERIFY['rainy_night'] = dict()
CONDITIONS_TO_VERIFY['rainy_night']['nb_img'] = [16, 24]
CONDITIONS_TO_VERIFY['rainy_night']['class_distrib'] = [13, 27]
CONDITIONS_TO_VERIFY['rainy_night']['nb_obj'] = [40, 60]
CONDITIONS_TO_VERIFY['rainy_night']['obj_size'] = [40, 60]

CONDITIONS_TO_VERIFY['snowy_day'] = dict()
CONDITIONS_TO_VERIFY['snowy_day']['nb_img'] = [18, 22]
CONDITIONS_TO_VERIFY['snowy_day']['class_distrib'] = [18, 23]
# Ignore this condition, in this context, most of the images has already more than 5 objects
CONDITIONS_TO_VERIFY['snowy_day']['nb_obj'] = [0, 100]
CONDITIONS_TO_VERIFY['snowy_day']['obj_size'] = [41, 59]

CONDITIONS_TO_VERIFY['snowy_night'] = dict()
CONDITIONS_TO_VERIFY['snowy_night']['nb_img'] = [15, 25]
CONDITIONS_TO_VERIFY['snowy_night']['class_distrib'] = [16, 24]
# Ignore this condition, in this context, most of the images has already more than 5 objects
CONDITIONS_TO_VERIFY['snowy_night']['nb_obj'] = [0, 100]
CONDITIONS_TO_VERIFY['snowy_night']['obj_size'] = [33, 67]

CONDITIONS_TO_VERIFY['sunny_day'] = dict()
CONDITIONS_TO_VERIFY['sunny_day']['nb_img'] = [15, 25]
CONDITIONS_TO_VERIFY['sunny_day']['class_distrib'] = [10, 25]
# Ignore this condition, in this context, most of the images has already more than 5 objects
CONDITIONS_TO_VERIFY['sunny_day']['nb_obj'] = [0, 100]
CONDITIONS_TO_VERIFY['sunny_day']['obj_size'] = [23, 77]


# Conditions to verify for synthetic data
CONDITIONS_TO_VERIFY['fog_day'] = dict()
CONDITIONS_TO_VERIFY['fog_day']['nb_img'] = [14, 26]
CONDITIONS_TO_VERIFY['fog_day']['class_distrib'] = [14, 26]
CONDITIONS_TO_VERIFY['fog_day']['nb_obj'] = [0, 100]
CONDITIONS_TO_VERIFY['fog_day']['obj_size'] = [29, 71]

CONDITIONS_TO_VERIFY['rain_day'] = dict()
CONDITIONS_TO_VERIFY['rain_day']['nb_img'] = [17, 23]
CONDITIONS_TO_VERIFY['rain_day']['class_distrib'] = [18, 22]
CONDITIONS_TO_VERIFY['rain_day']['nb_obj'] = [40, 60]
CONDITIONS_TO_VERIFY['rain_day']['obj_size'] = [39, 61]


def get_context_path_dict():
    """
    This function generates a dict where the keys are the context_time and the values are a list of all the path of the
    datasets that contain images of this context_time
    :return: The dict generated
    """
    context_dict = dict()
    for dataset in DATASETS:
        if dataset not in DATASET_SYNTHETIC:
            if dataset in DATASETS_INTERNE:
                dataset_path = "{}/DATASET_INTERNE/{}/*caipy/".format(DESKTOP_PATH, dataset)
            else:
                dataset_path = "{}/DATASET_EXTERNE/{}/*caipy/".format(DESKTOP_PATH, dataset)
            for file in glob.glob(dataset_path):
                context_time = file.split("/")[-2].split("_")[-3:-1]
                if context_time[0] in WEATHER:
                    context_time_key = "{}_{}".format(context_time[0], context_time[1])
                else:
                    if context_time[1] == "night":
                        context_time_key = "{}_{}".format("clear", context_time[1])
                    elif context_time[1] == "interior":
                        context_time_key = context_time[1]
                    else:
                        context_time_key = "{}_{}".format(context_time[1], "day")

                if context_time_key in context_dict.keys():
                    context_dict[context_time_key].append(file)
                else:
                    context_dict[context_time_key] = [file]
    return context_dict


def generate_evaluation_dataset(contexts, context_dict):
    """
    For the context given, generates a train and test set with 20% of images for the datasets without batch and 20%
    of the batches for datasets with batches
    :param contexts: the context we want to generate a train/test set
    :param context_dict: the dictionary where the keys are the context and the values all the annotations path that
    where the images are in this context
    :return:
    """
    annotation_data = dict()
    annotation_data['train'] = dict()
    annotation_data['test'] = dict()

    # For the datasets that are not composed of batches
    images = []
    annotations = []
    for path in context_dict[contexts]:
        for annotation_path in glob.glob("{}Annotations_caipy/*.json".format(path)):
            annotations.append(annotation_path)
        for image_path in glob.glob("{}Images/*.jpg".format(path)) + glob.glob("{}/Images/*.png".format(path)):
            images.append(image_path)
    if len(images) != 0:
        X_train, X_test, y_train, y_test = train_test_split(images, annotations, test_size=0.2)
        annotation_data["test"][contexts] = y_test
        annotation_data["train"][contexts] = y_train
    else:
        annotation_data["test"][contexts] = []
        annotation_data["train"][contexts] = []

    # For the datasets that are composed of batches
    # We add to the eval dataset all the images from the same batch
    batch_images = []
    batch_annotations = []
    for path in context_dict[contexts]:
        for image_path in glob.glob("{}Images/*/".format(path)):
            batch_images.append(image_path)
        for annotation_path in glob.glob("{}Annotations_caipy/*/".format(path)):
            batch_annotations.append(annotation_path)
    if len(batch_images) != 0:
        X_train, X_test, y_train, y_test = train_test_split(batch_images, batch_annotations, test_size=0.2)
        for batch in y_test:
            for annotation_path in glob.glob("{}*".format(batch)):
                annotation_data["test"][contexts].append(annotation_path)
        for batch in y_train:
            for annotation_path in glob.glob("{}*".format(batch)):
                annotation_data["train"][contexts].append(annotation_path)

    # Save everything into a json file
    test_file = "{}_test.json".format(contexts)
    train_file = "{}_train.json".format(contexts)
    with open(test_file, 'w') as outfile:
        print("writing test files to :", test_file)
        json.dump(annotation_data["test"][contexts], outfile)
    with open(train_file, 'w') as outfile:
        print("writing train files to :", train_file)
        json.dump(annotation_data["train"][contexts], outfile)


def check_distribution_of_class_context(contexts):
    """
    This functions check if the json file for the context that contain all the annotations that will be used for the
    evaluation set verifies the distribution of class conditions.
        The conditions are : for each context :
        - Number of images : a1 <= nb_images <= b1 of the complete train/test set
        - Number of bbox sedan : a2 <= nb_bbox_sedan <= b2 of the total number of sedan bbox in the context
        - Number of bbox person : a3 <= nb_bbox_person <= b3 of the total number of person bbox in the context
    Those two classes are the most important, we will only focus on it to create our eval set.
    :param contexts: the context we want to check
    :return: A boolean True if the test set verifies the distribution of class conditions, False otherwise
    """
    class_to_check = ["sedan", "person"]
    ratio_stat = get_eval_ratio(contexts)
    good_context_distribution = True
    if not CONDITIONS_TO_VERIFY[contexts]['nb_img'][0] <= ratio_stat["nb_images"] \
           <= CONDITIONS_TO_VERIFY[contexts]['nb_img'][1]:
        good_context_distribution = False

    for key in class_to_check:
        if key in ratio_stat.keys():
            if not CONDITIONS_TO_VERIFY[contexts]['class_distrib'][0] <= ratio_stat[key] \
                   <= CONDITIONS_TO_VERIFY[contexts]['class_distrib'][1]:
                good_context_distribution = False

    if good_context_distribution:
        print("\nGood distribution")
    else:
        print("\nBad distribution")

    print("nb_images", ratio_stat['nb_images'])
    for key in class_to_check:
        if key in ratio_stat.keys():
            print(key, ratio_stat[key])

    if good_context_distribution:
        return True
    return False


def check_distribution_of_number_of_object(contexts):
    """
    Check if the evaluation set for the context given, verifies the number of object condition.
    We have nb_obj the number of bbox in an image, we want this percentage of images with less or more than 5 objects
    be in an interval [a1, b1] to verify this condition
    :param contexts: the context we want to evaluate
    :return: True if the condition is verified, False otherwise
    """
    number_of_obj_ratio = get_number_of_object_statistics(contexts, "test")
    if CONDITIONS_TO_VERIFY[contexts]['nb_obj'][0] <= number_of_obj_ratio['less_5'] \
            <= CONDITIONS_TO_VERIFY[contexts]['nb_obj'][1]:
        good_nb_obj_distribution = True
    else:
        good_nb_obj_distribution = False

    if good_nb_obj_distribution:
        print("Good number of object distribution")
    else:
        print("Bad number of object distribution")

    print("Less than 5 objects :", number_of_obj_ratio['less_5'])
    print("More than 5 objects :", number_of_obj_ratio['more_5'])

    return good_nb_obj_distribution


def check_distribution_size_of_object(contexts):
    """
    Check if the evaluation set for the context given verifies the size of object condition
    We want the percentage of bbox in the set that is "big" be inside an interval [a1, b1]
    :param contexts: the context we want to evaluate
    :return: True if the condition is verified, False otherwise
    """
    size_of_obj_ratio = get_size_of_object_statistics(contexts, "test")
    if CONDITIONS_TO_VERIFY[contexts]['obj_size'][0] <= size_of_obj_ratio['big'] \
            <= CONDITIONS_TO_VERIFY[contexts]['obj_size'][1]:
        good_nb_obj_distribution = True
    else:
        good_nb_obj_distribution = False

    if good_nb_obj_distribution:
        print("Good number of object distribution")
    else:
        print("Bad number of object distribution")

    print("Big objects :", size_of_obj_ratio['big'])
    print("Small objects :", size_of_obj_ratio['small'])

    return good_nb_obj_distribution


def generate_until_good_distribution(contexts):
    """
    this function will generate an evaluation set until the conditions are verified for a specific context
    :param contexts: The context we want to generate an evaluation set
    :return:
    """
    context_path_dict = get_context_path_dict()
    good_class_distribution = False
    good_nb_obj_distribution = False
    good_size_obj_distribution = False
    while not (good_class_distribution and good_nb_obj_distribution and good_size_obj_distribution):
        print("\nGenerating a new evaluation set for the context {} ...".format(contexts))
        generate_evaluation_dataset(contexts, context_path_dict)
        good_class_distribution = check_distribution_of_class_context(contexts)
        good_nb_obj_distribution = check_distribution_of_number_of_object(contexts)
        good_size_obj_distribution = check_distribution_size_of_object(contexts)


if __name__ == "__main__":
    print(CONTEXTS)
    context = input()
    generate_until_good_distribution(context)
