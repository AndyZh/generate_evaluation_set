#!/bin/env python3
import glob
import json
import os
DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset/Real/"
os.chdir(DATASET_DIRECTORY)


def get_all_eval_set_list():
    """
    This function retrieve all the json files that contains the list of the eval data for each context
    :return: the list of the json file for every context that contains all the evaluation data
    """
    context_eval = []
    for file in glob.glob("{}file_list/*test.json".format(DATASET_DIRECTORY)):
        context_eval.append(file)
    return context_eval


def copy_data_context(data_list, context):
    """
    Copy the data (image/annotations) of the eval set into a new folder
    This folders created will be uploaded in NAS
    :param data_list: directory where the .json file that contain the list of data is saved
    :param context: the context we consider
    :return:
    """
    print("Copying images and annotation of the evaluation set for the context :", context, "...")
    with open(data_list, "r") as current_annotation:
        annotations_list = json.load(current_annotation)

    for annotation_path in annotations_list:
        # Get all the need path for the copy
        image_path = annotation_path.replace("Annotations_caipy", "Images")
        image_path_jpg = image_path.replace(".json", ".jpg")
        image_path_png = image_path.replace(".json", ".png")

        copy_annotation_command = "cp {} data/{}/Annotations/".format(annotation_path, context)
        # Check first if exists, if yes do copy
        # We need to do this because of the images that are in jpg format and the other that are in png format
        copy_img_jpg_command = "test -f {} && cp {} data/{}/Images/".format(image_path_jpg, image_path_jpg, context)
        copy_img_png_command = "test -f {} && cp {} data/{}/Images/".format(image_path_png, image_path_png, context)

        # We copy everything
        os.system(copy_annotation_command)
        os.system(copy_img_jpg_command)
        os.system(copy_img_png_command)


def create_context_folders(context):
    """
    This function will create all the folders' context/ context/Images/ and context/Annotations/ where the evaluation
    data will be copied
    :param context: the context we consider
    :return:
    """
    context_dir = "data/{}".format(context)
    images_dir = "data/{}/Images".format(context)
    annotations_dir = "data/{}/Annotations".format(context)
    os.mkdir(context_dir)
    os.mkdir(images_dir)
    os.mkdir(annotations_dir)


def copy_all_data():
    """
    This function will use copy_data_context for all the context we consider
    :return:
    """
    os.system("rm -r data")
    os.mkdir("data")
    context_eval = get_all_eval_set_list()
    for data_list in context_eval:
        context = data_list.split("/")[-1].replace("_test.json", "")
        create_context_folders(context)
        copy_data_context(data_list, context)


if __name__ == "__main__":
    copy_all_data()
