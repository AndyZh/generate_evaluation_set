#!/bin/env python3
import sys
import os
from generate_eval_set import get_context_path_dict

sys.path.insert(1, "/home/andyzhang/convert_data_to_caipy")

DESKTOP_PATH = "/home/andyzhang/Bureau/complete_dataset"


def get_all_data():
    """
    This function generates a dict where the keys is "sunny_day" and the values are a list of all the path of the
    datasets that contain images of this context
    :return: The dict containing the sunny day data
    """
    context = "sunny_day"
    context_dict = get_context_path_dict()
    weather_kitti_sunny = "/home/andyzhang/Bureau/complete_dataset/DATASET_EXTERNE/" \
                          "weather_kitti/weather_kitti_rain_0_caipy/"
    foggy_cityscapes_sunny = "/home/andyzhang/Bureau/complete_dataset/DATASET_EXTERNE/" \
                              "foggy_cityscapes/foggy_cityscapes_fog_0_caipy/"

    context_dict[context].append(weather_kitti_sunny)
    context_dict[context].append(foggy_cityscapes_sunny)
    return context_dict


def copy_data(directory, context_dict, context):
    """
    This function creates a folder containing Images/Annotations of all the images in the directory
    Args:
        directory: The output directory where the dataset of sunny images will be copied
        context_dict: The context dict containing all the contextual data folder
        context : The context
    :return:
    """
    # First creates the dataset folders
    # dataset_dir = f"{directory}/{context}"
    # images_dir = f"{directory}/{context}/Images"
    # annotations_dir = f"{directory}/{context}/Annotations"

    dataset_dir = f"{directory}/complete_training_data"
    images_dir = f"{directory}/complete_training_data/Images"
    annotations_dir = f"{directory}/complete_training_data/Annotations"

    # os.system(f"rm -r {dataset_dir}")
    # os.mkdir(dataset_dir)
    # os.mkdir(images_dir)
    # os.mkdir(annotations_dir)

    for dataset in context_dict[context]:
        dataset_name = dataset.split("/")[-2]
        if dataset_name == "bdd_100k_sunny_day_caipy" or dataset_name == "weather_kitti_rain_0_caipy" or dataset_name == "bdd_100k_clear_night_caipy":
            images_copy_command = f'find {dataset}Images -type f -exec cp {{}} {images_dir} \\;'
            annotations_copy_command = f'find {dataset}Annotations_caipy -type f -exec cp {{}} {annotations_dir} \\;'
        else:
            images_copy_command = f"find {dataset}Images -type f -exec cp {{}} {images_dir} \\;"
            annotations_copy_command = f"find {dataset}Annotations_caipy -type f -exec cp {{}} {annotations_dir} \\;"
        print(f"Copying {dataset_name} ..")
        os.system(images_copy_command)
        os.system(annotations_copy_command)


if __name__ == "__main__":
    context_dict = get_all_data()

    dataset_directory = "/home/andyzhang/Bureau"
    copy_data(dataset_directory, context_dict)
