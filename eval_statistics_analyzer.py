#!/bin/env python3

import pandas as pd
import os
import dataset_statistics

CONTEXTS = ["clear_night", "foggy_day", "foggy_night", "interior", "rainy_day", "rainy_night", "snowy_day",
            "snowy_night", "sunny_day"]

SYNTHETIC_CONTEXTS = ["rain_day", "fog_day"]

IGNORE = ["ignore areas", "ignored class"]


def show_statistics(eval_stat_ratio, global_stat, dataset_stat, nb_obj_stat, size_stat, view_stat):
    """
    This function show the statistics of the evaluation set for a given context
    :param eval_stat_ratio: the dict containing the ratio stats
    :param global_stat: the dict containing the global statistics
    :param dataset_stat: the dict containing the evaluation set statistics
    :param view_stat: the dict containing the view statistics of the evaluation
    :param nb_obj_stat: the dict containing the statistics of the number of objects
    :param size_stat: the dict containing the statistics of the size of the objects
    :return:
    """
    print("-------------------------------------------------------------------------------------------------------")
    stats = []
    keys = [key for key in eval_stat_ratio.keys() if key not in IGNORE and key != "nb_images"]

    stats.append([dataset_stat["nb_images"], global_stat["nb_images"],
                  "{}%".format(round(eval_stat_ratio["nb_images"], 2))])
    nb_images_stats = pd.DataFrame(stats, columns=['Number of images in eval', '  Number of image in dataset',
                                                   '  percentage of the dataset'], index=["nb_images"])
    print(nb_images_stats)
    print("-------------------------------------------------------------------------------------------------------")

    stats = []
    for key in dataset_stat.keys():
        if key not in IGNORE and key != "nb_images":
            stats.append([dataset_stat[key], global_stat[key], "{}%".format(round(eval_stat_ratio[key], 2))])
    class_stats = pd.DataFrame(stats, columns=['Number of bbox in eval', '  Number of bbox in dataset',
                                               '  percentage of the dataset'], index=keys)
    print(class_stats)

    nb_bbox = 0
    nb_images = dataset_stat["nb_images"]
    for key in dataset_stat.keys():
        if key != "nb_images" and key not in IGNORE:
            nb_bbox += dataset_stat[key]

    for stat in [nb_obj_stat, size_stat, view_stat]:
        print("-------------------------------------------------------------------------------------------------------")

        keys = stat.keys()
        if stat == size_stat:
            obj_stats = [[int(val * nb_bbox / 100), "{}%".format(round(float(val), 2))] for val in stat.values()]
            obj_stats = pd.DataFrame(obj_stats, columns=["Number of bounding box",
                                                         "      Proportion"], index=keys)
        else:
            obj_stats = [[int(val * nb_images / 100), "{}%".format(round(float(val), 2))] for val in stat.values()]
            obj_stats = pd.DataFrame(obj_stats, columns=["Number of images",
                                                         "      Proportion"], index=keys)
        print(obj_stats)


if __name__ == "__main__":
    print("Real data :", CONTEXTS)
    print("Synthetic data :", SYNTHETIC_CONTEXTS)
    context = input()

    if context in SYNTHETIC_CONTEXTS:
        DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset/Synthetic/file_list"
    else:
        DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset/Real/file_list"
    os.chdir(DATASET_DIRECTORY)

    eval_stats = dataset_statistics.get_eval_ratio(context)
    global_stats = dataset_statistics.get_global_statistics(context)

    nb_obj_stats = dataset_statistics.get_number_of_object_statistics(context, "test")
    size_stats = dataset_statistics.get_size_of_object_statistics(context, "test")
    view_stats = dataset_statistics.get_view_statistics(context, "test")

    dataset_statistic = dataset_statistics.get_context_statistics(context, "test")

    show_statistics(eval_stats, global_stats, dataset_statistic, nb_obj_stats, size_stats, view_stats)
