#!/usr/bin/env python3

import json
import os
import glob

from generate_training_data_utilities import get_all_data, copy_data
from libia.dataset import from_caipy
from libia.utils import grouper
import pandas as pd
import seaborn
import matplotlib.pyplot as plt
from ot import emd2, sinkhorn2, dist
from reset_image_ids import reset_image_ids
from reset_object_ids import reset_object_image_ids
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=RuntimeWarning)

DATASETS_DIRECTORY = "/home/andyzhang/Bureau"
BINS = range(0, 600, 50)


def remove_evaluation_data(context):
    """
    Removes from the dataset all the images that are used in the benchmark dataset
    :return:
    """
    dataset_path = f"{DATASETS_DIRECTORY}/complete_training_data"
    if context == "sunny_day":
        eval_list = [
            f"/home/andyzhang/Bureau/Benchmark_dataset/Real/file_list/{context}_test.json",
            "/home/andyzhang/Bureau/Benchmark_dataset/Synthetic/file_list/fog_day_test.json",
            "/home/andyzhang/Bureau/Benchmark_dataset/Synthetic/file_list/rain_day_test.json"
        ]
    else:
        eval_list = [
            f"/home/andyzhang/Bureau/Benchmark_dataset/Real/file_list/{context}_test.json"
        ]

    for eval_list in eval_list:
        with open(eval_list, "r") as current_annotation:
            annotations_dict = json.load(current_annotation)
        for annotation in annotations_dict:
            annotation_to_delete = annotation.split("/")[-1]
            image_to_delete = annotation_to_delete.replace("json", "jpg")
            delete_annotation_command = f"rm {dataset_path}/Annotations/{annotation_to_delete}"
            delete_image_command = f"rm {dataset_path}/Images/{image_to_delete}"
            print(f"Deleting {image_to_delete} from {context} training dataset ...")
            os.system(delete_image_command)
            os.system(delete_annotation_command)


def remove_images_with_ignore_areas(context):
    """
    Removes from the sunny_day dataset all the images that contains ignore areas
    Args:
        context: the context
    :return:
    """
    for annotation in glob.glob(f"{DATASETS_DIRECTORY}/complete_training_data/Annotations/*"):
        to_delete = False
        with open(annotation, "r") as current_annotation:
            annotations_dict = json.load(current_annotation)
        for obj in annotations_dict['annotations']:
            if obj["category_str"] == "ignore areas":
                to_delete = True
        if to_delete:
            image_to_delete = annotation.replace("json", "jpg").replace("Annotations", "Images")
            print(f"Deleting {image_to_delete} from sunny_day training dataset (contains ignore areas) ...")

            delete_image_command = f"rm {image_to_delete}"
            delete_annotation_command = f"rm {annotation}"

            os.system(delete_image_command)
            os.system(delete_annotation_command)


def keep_car_person_in_annotations(dataset_annotations):
    """
    This function removes from the annotations all the classes that are not car or person
    :param dataset_annotations: the list of annotation
    :return:
    """
    to_keep = ["person", "car"]
    for annotation_path in dataset_annotations:
        with open(annotation_path, "r") as current_annotation:
            annotations_dict = json.load(current_annotation)

        kept_objects = []
        for obj in annotations_dict['annotations']:
            if obj['category_str'] in to_keep:
                kept_objects.append(obj)
        annotations_dict['annotations'] = kept_objects

        with open(annotation_path, "w") as outfile:
            print("Keeping cars and persons in :", annotation_path)
            json.dump(annotations_dict, outfile)


def add_night_in_annotations():
    """
    Modifies the annotations of the night generated images : image name, context, synthetic
    :return:
    """
    for annotation in glob.glob(f"{DATASETS_DIRECTORY}/sunny_day/Annotations/*"):
        with open(annotation, "r") as current_annotation:
            annotations_dict = json.load(current_annotation)

        # File name
        new_file_name = annotations_dict['image']['file_name'].replace(".jpg", "_night.jpg")
        annotations_dict['image']['file_name'] = new_file_name

        # Weather
        annotations_dict['image']['tags']['weather'] = 'clear'

        # Time
        annotations_dict['image']['tags']['time'] = 'night'

        # Image type
        annotations_dict['image']['tags']['image_type'] = 'synthetic'

        with open(annotation, "w") as outfile:
            print("Modifying annotations in :", annotation)
            json.dump(annotations_dict, outfile)


def check_split_conformity(dataset):
    plt.figure()
    seaborn.histplot(dataset.annotations, x="box_height", stat="density", hue="split", common_norm=False, bins=BINS)

    plt.figure()
    seaborn.histplot(dataset.annotations, x="category_str", stat="density", hue="split", common_norm=False)
    plt.xticks(rotation=45)
    plt.savefig(f"{DATASETS_DIRECTORY}/libia_split/class_distrib.jpg", dpi=500, bbox_inches = 'tight')

    person_sedan = dataset.annotations[dataset.annotations["category_str"].isin(["person", "sedan"])]
    plt.figure()
    seaborn.displot(person_sedan, x="box_height", col="category_str", hue="split", stat="density", common_norm=False,
                    bins=BINS)
    plt.savefig(f"{DATASETS_DIRECTORY}/libia_split/bbox_height_distrib.jpg", dpi=500, bbox_inches = 'tight')

    cutted = pd.cut(dataset.annotations["box_height"], bins=BINS).apply(lambda x: x.left).astype(float)
    target_hist = dataset.annotations.groupby(cutted).size()
    for (split, group) in dataset.annotations.groupby("split"):
        cutted = pd.cut(group["box_height"], bins=BINS).apply(lambda x: x.left).astype(float)
        candidate_hist = dataset.annotations.groupby(cutted).size()
        left_bins = target_hist.index.to_numpy()
        right_bins = candidate_hist.index.to_numpy()
        distance_matrix = dist(left_bins[:, None], right_bins[:, None], metric="cityblock")
        distance_matrix = distance_matrix / distance_matrix.max()

        normalized_left = (target_hist / target_hist.sum()).to_numpy()
        normalized_right = (candidate_hist / candidate_hist.sum()).to_numpy()
        emd = emd2(normalized_left, normalized_right, distance_matrix)
        sinkhorn = sinkhorn2(normalized_left, normalized_right, distance_matrix, reg=1e-1)
        print(f"EMD and sinkhorn between target and {split} for box height : {emd} {sinkhorn}")


def train_valid_split_with_libia():
    """
    Splits the dataset into a training and a validation set using libia to balance the class distribution
    The split is 90-10% for training-valid
    :return:
    """
    current_dataset = from_caipy(f"{DATASETS_DIRECTORY}/complete_training_data/")

    box_height_group = grouper.ContinuousGroup("box_height", BINS)
    splitted_data = current_dataset.split(input_seed=1,
                                     split_names=["train", "valid"],
                                     target_split_shares=[0.9, 0.1],
                                     keep_separate_groups=["image_id"],
                                     keep_balanced_groups=["category_id", box_height_group],
                                     earth_mover_regularization=0)
    os.system(f"rm -rf {DATASETS_DIRECTORY}/libia_split")
    os.mkdir(f"{DATASETS_DIRECTORY}/libia_split")
    splitted_data.to_yolov5(f"{DATASETS_DIRECTORY}/libia_split", copy_images=True)

    check_split_conformity(splitted_data)


def refresh_output_data_folder():
    """
    This functions deletes and recreate the output folder
    """
    dataset_path = "/home/andyzhang/Bureau/complete_training_data"
    os.system(f"rm -r {dataset_path}")

    os.mkdir(dataset_path)
    os.mkdir(f"{dataset_path}/Images")
    os.mkdir(f"{dataset_path}/Annotations")


if __name__ == "__main__":

    # contexts = ['clear_night']
    # training_data = get_all_data()

    # refresh_output_data_folder()
    
    # for context in contexts:
    #     dataset_path = f"{DATASETS_DIRECTORY}/{context}"
    
    #     # First, get all the sunny day dataset into the same dataset
    #     copy_data(DATASETS_DIRECTORY, training_data, context)

    #     # Remove all the images/annotations that are used in the evaluation set
    #     remove_evaluation_data(context)

    #     # Remove all the images with ignore areas
    #     remove_images_with_ignore_areas(context)

    # dataset_annotations = []
    # for annotation in glob.glob(f"{DATASETS_DIRECTORY}/complete_training_data/Annotations/*"):
    #     dataset_annotations.append(annotation)

    # # Reset the image annotations
    # reset_image_ids(dataset_annotations)

    # # Keep only person and car in annotations
    # keep_car_person_in_annotations(dataset_annotations)

    train_valid_split_with_libia()