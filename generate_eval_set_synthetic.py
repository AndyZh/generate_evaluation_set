#!/bin/env python3
import sys
import glob

sys.path.insert(1, "/home/andyzhang/convert_data_to_caipy")
from generate_eval_set import generate_evaluation_dataset, check_distribution_size_of_object, \
    check_distribution_of_number_of_object, check_distribution_of_class_context

DESKTOP_PATH = "/home/andyzhang/Bureau"

# The datasets that contains synthetic images
DATASET_SYNTHETIC = ["foggy_cityscapes", "weather_kitti"]

# Contexts
CONTEXTS = ["rain_day", "fog_day"]

# Set of conditions for a good distributed dataset
CONDITIONS_TO_VERIFY = dict()

# Because of the amount of data and the diversity of the datasets, some conditions will be harder to verify depending
# on the contexts. The values are intervals where the values needs to be in to verify the conditions
# - nb_img : condition of the percentage of the number of image we take for the eval
# - class_distrib : condition of the percentage of the bbox for the class sedan and person
# - nb_obj : condition of the percentage of images will less than 5 objects
# - obj_size : condition on the number of bbox that are "big"


def get_context_path_dict_synth():
    """
    This function generates a dict for the dataset that contains synthetic data where the keys are the context_time
    and the values are a list of all the path of the datasets that contain images of this context_time
    :return: The dict generated
    """
    context_dict = dict()
    for dataset in DATASET_SYNTHETIC:
        dataset_path = "{}/DATASET_EXTERNE/{}/*_0_caipy/".format(DESKTOP_PATH, dataset)
        for file in glob.glob(dataset_path):
            context_time = file.split("/")[-2].split("_")[-3:-1]
            context_time_key = "{}_{}".format(context_time[0], "day")
            if context_time_key in context_dict.keys():
                context_dict[context_time_key].append(file)
            else:
                context_dict[context_time_key] = [file]

    return context_dict


def generate_until_good_distribution_synth(contexts):
    """
    this function will generate an evaluation set until the conditions are verified for a specific context
    :param contexts: The context we want to generate an evaluation set
    :return:
    """
    context_path_dict = get_context_path_dict_synth()
    good_class_distribution = False
    good_nb_obj_distribution = False
    good_size_obj_distribution = False
    while not (good_class_distribution and good_nb_obj_distribution and good_size_obj_distribution):
        print("\nGenerating a new evaluation set for the context {} ...".format(contexts))
        generate_evaluation_dataset(contexts, context_path_dict)
        good_class_distribution = check_distribution_of_class_context(contexts)
        good_nb_obj_distribution = check_distribution_of_number_of_object(contexts)
        good_size_obj_distribution = check_distribution_size_of_object(contexts)


if __name__ == "__main__":
    print(CONTEXTS)
    context = input()
    generate_until_good_distribution_synth(context)
